package geometry;

public class Square {
    private int width;
    private int height;

    public Square(int width, int height){
        this.width=width;
        this.height=height;
    }

    public int getArea(){
        return this.width*this.height;
    }

    public String toString(){
        return "width: " + this.width + " height: " + this.height;
    }
}
