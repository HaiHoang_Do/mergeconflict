package geometry;

public class Triangle {
    private int side;
    private int height;
    private int base;

    public Triangle(int side, int height, int base)
    {
        this.side = side;
        this.height = height;
        this.base = base;
    }

    public String toString()
    {
        String returnString = "Side: " + this.side + " Height: " + this.height + " Base: " + this.base;
        return returnString;
    }

    public int getArea()
    {
        int area = (base*height)/2;
        return area;
    }
}
