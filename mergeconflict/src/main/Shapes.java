
public class Shapes {
    public static void main(String[] args)
    {
        Square sqr = new Square(3,3);

        System.out.println(sqr.getArea());
        System.out.println(sqr.toString());

        Triangle t1 = new Triangle(10, 4, 94);
        System.out.println(t1.getArea());
    }
}
